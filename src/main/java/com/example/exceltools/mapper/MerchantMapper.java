package com.example.exceltools.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exceltools.entity.Merchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MerchantMapper extends BaseMapper<Merchant> {

    void batchInsertMeta(@Param("list") List<Merchant> merchantList);

    List<Merchant> getMerchant();
}
