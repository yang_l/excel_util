package com.example.exceltools.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.exceltools.exceltools.ExcelExport;
import com.example.exceltools.exceltools.ExcelImport;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author yang_li
 */
@Data
@TableName("merchant")
public class Merchant {

    @ExcelExport("编号")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ExcelImport("姓名")
    @ExcelExport("姓名")
    @TableField(value = "name")
    private String name;

    @ExcelImport("地址")
    @ExcelExport("地址")
    @TableField(value = "address")
    private String address;

    /**
     * 商户类型:1:餐饮娱乐、2:交通出行、3:文化旅游、4:政务缴费、5:教育培训、6:便民缴费、7:医疗健康、8:零售便利
     */
    @ExcelImport(value = "商户类型", kv = "1-餐饮娱乐;2-交通出行;3-文化旅游;4-政务缴费;5-教育培训;6-便民缴费;7-医疗健康;8-零售便利;")
    @ExcelExport(value = "商户类型")//, kv = "1-餐饮娱乐;2-交通出行;3-文化旅游;4-政务缴费;5-教育培训;6-便民缴费;7-医疗健康;8-零售便利;"
    @TableField(value = "type")
    private Integer type;

    @ExcelImport("电话")
    @ExcelExport("电话")
    @TableField(value = "phone_num")
    private String phoneNum;

    /**
     * 商户状态 0：未启用  1：已启用
     */
    @ExcelImport(value = "商户状态", kv = "0-未启用;1-以启用;")
    @ExcelExport(value = "商户状态")   //, kv = "0-未启用;1-以启用;"
    @TableField(value = "status")
    private Integer status;

    @ExcelImport("封面")
    @ExcelExport("封面")
    @TableField(value = "cover")
    private String cover;

    @ExcelImport("经度")
    @ExcelExport("经度")
    @TableField("longitude")
    private Double longitude;

    @ExcelImport("纬度")
    @ExcelExport("纬度")
    @TableField("latitude")
    private Double latitude;

    @ExcelImport("创建时间")
    @ExcelExport("创建时间")
    @TableField(value = "create_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ExcelImport("更新时间")
    @ExcelExport("更新时间")
    @TableField(value = "update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 删除标识 0：没删除 1：已删除
     */
    @ExcelImport("删除标识")
    @ExcelExport("删除标识")
    @TableField(value = "is_delete")
    private Integer isDelete;

    @ExcelImport("营业时间")
    @ExcelExport("营业时间")
    @TableField("business_hour")
    private String businessHour;

    /**
     * 商户的唯一标识（存于redis中）
     */
    @ExcelImport("唯一标识")
    @ExcelExport("唯一标识")
    @TableField("uuid")
    private String uuid;

    @ExcelImport("权重")
    @ExcelExport("权重")
    @TableField("score")
    private Integer score;

    @ExcelImport("测试 下拉框")
    @ExcelExport("测试 下拉框")
    @TableField("score")
    private List<String> test;

}
