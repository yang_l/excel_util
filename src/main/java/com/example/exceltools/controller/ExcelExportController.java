package com.example.exceltools.controller;

import com.example.exceltools.entity.Merchant;
import com.example.exceltools.exceltools.ExcelUtils;
import com.example.exceltools.mapper.MerchantMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@RestController
@RequestMapping("excel")
public class ExcelExportController {

    @Resource
    private MerchantMapper merchantMapper;

    /**
    导出自定义的表格   手动构建用户数据
     */
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws MalformedURLException {

        // 表头数据
        List<List<Object>> sheetDataList = getLists();

        // 导出数据
        ExcelUtils.export(response, "用户表", sheetDataList);
    }

    /**
    导出自定义的表格   手动构建用户数据  数据可以带下拉框
     */
    @GetMapping("/exportDropDownBox")
    public void exportDropDownBox(HttpServletResponse response) throws MalformedURLException {

        // 表头数据
        List<List<Object>> sheetDataList = getLists();

        // 可以将导出的数据设置为有下拉框选项的格式
        Map<Integer, List<String>> map = new HashMap<>(1);
        map.put(2, Arrays.asList("男", "女"));
        map.put(3, Arrays.asList("成都", "北京", "shanghai1", "guangzhou"));

        // 导出数据
        ExcelUtils.export(response, "用户表", sheetDataList,map);
    }

    /**
    导出自定义的表格   手动构建用户数据  横向合并最后一个单元格
     */
    @GetMapping("/exportMerge")
    public void exportMerge(HttpServletResponse response) {

        List<Object> head = Arrays.asList("姓名", "年龄", "性别", "地址", ExcelUtils.COLUMN_MERGE);

        // 用户1数据 // user中的数据可以为 ”“  但是不能为null
        List<Object> user1 = new ArrayList<>();
        user1.add("诸葛亮");
        user1.add(60);
        user1.add("男");
        user1.add("chengdu1");
        user1.add("gaoxin");

        // 用户2数据
        List<Object> user2 = new ArrayList<>();
        user2.add("大乔");
        user2.add(28);
        user2.add("女");
        /*user2.add("https://profile.csdnimg.cn/6/1/9/0_m0_48717371"); // 显示链接*/
        user2.add("");
        user2.add("gaoxin");
        // 将数据汇总
        List<List<Object>> sheetDataList = new ArrayList<>();
        sheetDataList.add(head);
        sheetDataList.add(user1);
        sheetDataList.add(user2);

        // 可以将导出的数据设置为有下拉框选项的格式
        Map<Integer, List<String>> map = new HashMap<>(2);
        map.put(2, Arrays.asList("男", "女"));
        map.put(3, Arrays.asList("成都", "北京", "shanghai", "guangzhou"));

        // 导出数据
        ExcelUtils.export(response, "用户表", sheetDataList,map);
    }


    /** 按实体类导出数据  导出到一个sheet中   记得在对应实体类上面加上注解  使用 @ExcelExport（"xxx"） 来定义导出的表头文字 */
    @GetMapping("/exportMerchant")
    public void exportMerchant(HttpServletResponse response) {
        List<Merchant> merchants = merchantMapper.getMerchant();
        merchants.forEach(item->item.setTest(Arrays.asList("chengdu","shanghai")));
        // 导出数据
        ExcelUtils.export(response, "用户表", merchants, Merchant.class);
    }


    /**
     这是一个构造数据的方法
      */
    private List<List<Object>> getLists() throws MalformedURLException {
        // 表头数据
        List<Object> head = Arrays.asList("姓名", "年龄", "性别","地址", "头像");
        // 用户1数据
        List<Object> user1 = new ArrayList<>();
        user1.add("诸葛亮");
        user1.add(60);
        user1.add("男");
        /* user1.add("https://profile.csdnimg.cn/A/7/3/3_sunnyzyq");  显示链接*/
        // 直接显示成图片
        user1.add("");
        user1.add(new URL("https://profile.csdnimg.cn/A/7/3/3_sunnyzyq"));

        // 用户2数据
        List<Object> user2 = new ArrayList<>();
        user2.add("大乔");
        user2.add(28);
        user2.add("女");
        user2.add("");
        // 显示链接
        user2.add("https://profile.csdnimg.cn/6/1/9/0_m0_48717371");
        // 将数据汇总
        List<List<Object>> sheetDataList = new ArrayList<>();
        sheetDataList.add(head);
        sheetDataList.add(user1);
        sheetDataList.add(user2);
        return sheetDataList;
    }


}
