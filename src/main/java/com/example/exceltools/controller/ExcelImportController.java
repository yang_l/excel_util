package com.example.exceltools.controller;

import com.alibaba.fastjson.JSONArray;
import com.example.exceltools.entity.Merchant;
import com.example.exceltools.exceltools.ExcelUtils;
import com.example.exceltools.mapper.MerchantMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author yang_li
 */
@RestController
@RequestMapping("import")
public class ExcelImportController {

    @Resource
    private MerchantMapper merchantMapper;

    /**
     将excel中的数据导入  数据库  记得使用注解和实体类映射起来  @ExcelImport("xxx")
     */
    @PostMapping("/import")
    public String importUser(@RequestPart("file") MultipartFile file) throws Exception {
        List<Merchant> merchants = ExcelUtils.readMultipartFile(file, Merchant.class);
//         将excel中的数据以 JSON的格式打印出来
        for (Merchant user : merchants) {
            System.out.println(user.toString());
        }
        merchantMapper.batchInsertMeta(merchants);

        return "{"+"code: 200"+"message: OK"+"}";
    }


    /*
    导入多sheet的excel表
     */
    @PostMapping("/importSheet")
    public void upload(@RequestPart("file") MultipartFile file) throws Exception {
        Map<String, JSONArray> map = ExcelUtils.readFileManySheet(file);
        map.forEach((key, value) -> {
            System.out.println("Sheet名称：" + key);
            System.out.println("Sheet数据：" + value);
            System.out.println("----------------------");
        });
    }

}
